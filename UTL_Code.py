####################################################
### REMOTE SENSING OF WATER QUALITY ON UTAH LAKE ###
####################################################

######################
### Prepare Images ###
######################

# Import modules.
import os
import pandas as pd

# Set working directory.
os.chdir(r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Code')

# Import remote sensing water quality module.
import rs_wq

# Create a mosaic raster file for each downloaded calibration image. The raster is stored in 16-bit unsigned to preserve the quality assessment band.
rs_wq.to_mosaic(inputdirectory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Calibration\Bulk Order 959943\Landsat 7 ETM_ C1 Level-1\Unzipped',outputdirectory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Calibration\Bulk Order 959943\Landsat 7 ETM_ C1 Level-1\Mosaics',sixteen_bit=True) # All in 7 ETM+.

# Create a mosaic raster file for each downloaded time image. The images are clipped to a shapefile of Utah Lake, and the raster is stored in 16-bit unsigned to preserve the quality assessment band.
rs_wq.to_mosaic(inputdirectory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Bulk Order 960324\Landsat 1-5 MSS C1 Level-1\Unzipped',outputdirectory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Bulk Order 960324\Landsat 1-5 MSS C1 Level-1\Mosaics',sixteen_bit=True,shapefile_to_clip_to=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Lake\UTL\UTL_Proper_Coordinates.shp') # For 1-5 MSS.
rs_wq.to_mosaic(inputdirectory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Bulk Order 960324\Landsat 4-5 TM C1 Level-1\Unzipped',outputdirectory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Bulk Order 960324\Landsat 4-5 TM C1 Level-1\Mosaics',sixteen_bit=True,shapefile_to_clip_to=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Lake\UTL\UTL_Proper_Coordinates.shp') # For 4-5 TM.
rs_wq.to_mosaic(inputdirectory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Bulk Order 960324\Landsat 7 ETM_ C1 Level-1\Unzipped',outputdirectory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Bulk Order 960324\Landsat 7 ETM_ C1 Level-1\Mosaics',sixteen_bit=True,shapefile_to_clip_to=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Lake\UTL\UTL_Proper_Coordinates.shp') # For 7 ETM+.
rs_wq.to_mosaic(inputdirectory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Bulk Order 960324\Landsat 8 OLI_TIRS C1 Level-1\Unzipped',outputdirectory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Bulk Order 960324\Landsat 8 OLI_TIRS C1 Level-1\Mosaics',sixteen_bit=True,shapefile_to_clip_to=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Lake\UTL\UTL_Proper_Coordinates.shp') # For 8 OLI & TIRS.

##############################################
### Extract Values from Calibration Images ###
##############################################

# Parameters for rs_wq.extract_values_at_points().
clip_shapefile=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Lake\UTL\UTL_Proper_Coordinates.shp'
points_shapefile='sample_pts_proper_projection.shp'
point_identifier='Monitoring'
mask_by_cloud=True
mask_by_NoData=0
window_size=3

# Excecute rs_wq.extract_values_at_points() for...

# June 22, 2009.
raster_1=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Calibration\Bulk Order 959943\Landsat 7 ETM_ C1 Level-1\Mosaics\LE07_L1TP_038032_20090622_20160917_01_T1.tif'
df_Jun_22=rs_wq.extract_values_at_points(raster=raster_1,points_shapefile=points_shapefile,point_identifier=point_identifier,clip_shapefile=clip_shapefile,mask_by_cloud=mask_by_cloud,mask_by_NoData=mask_by_NoData,window_size=window_size)
df_Jun_22.rename(columns={'Monitoring':'MonitoringLocationIdentifier'},inplace=True) # Rename single column.

# July 8, 2009.
raster_2=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Calibration\Bulk Order 959943\Landsat 7 ETM_ C1 Level-1\Mosaics\LE07_L1TP_038032_20090708_20160917_01_T1.tif'
df_Jul_08=rs_wq.extract_values_at_points(raster=raster_2,points_shapefile=points_shapefile,point_identifier=point_identifier,clip_shapefile=clip_shapefile,mask_by_cloud=mask_by_cloud,mask_by_NoData=mask_by_NoData,window_size=window_size)
df_Jul_08.rename(columns={'Monitoring':'MonitoringLocationIdentifier'},inplace=True) # Rename single column.

# August 9, 2009.
raster_3=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Calibration\Bulk Order 959943\Landsat 7 ETM_ C1 Level-1\Mosaics\LE07_L1TP_038032_20090809_20160917_01_T1.tif'
df_Aug_09=rs_wq.extract_values_at_points(raster=raster_3,points_shapefile=points_shapefile,point_identifier=point_identifier,clip_shapefile=clip_shapefile,mask_by_cloud=mask_by_cloud,mask_by_NoData=mask_by_NoData,window_size=window_size)
df_Aug_09.rename(columns={'Monitoring':'MonitoringLocationIdentifier'},inplace=True) # Rename single column.

# September 10, 2009.
raster_4=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Calibration\Bulk Order 959943\Landsat 7 ETM_ C1 Level-1\Mosaics\LE07_L1TP_038032_20090910_20160916_01_T1.tif'
df_Sept_10=rs_wq.extract_values_at_points(raster=raster_4,points_shapefile=points_shapefile,point_identifier=point_identifier,clip_shapefile=clip_shapefile,mask_by_cloud=mask_by_cloud,mask_by_NoData=mask_by_NoData,window_size=window_size)
df_Sept_10.rename(columns={'Monitoring':'MonitoringLocationIdentifier'},inplace=True) # Rename single column.

########################################
### Join Field and Spectral Datasets ###
########################################

# Read in field data as a Pandas data frame.
df_field=pd.read_csv('final_dataset.csv')

# Get field data frame column names.
print df_field.columns

# Keep only certain fields in the field data frame.
df_field=df_field[['date','MonitoringLocationIdentifier','temp_C','chl_ugL','susp_mgL']]

# Check field data frame column names again.
print df_field.columns

# Subset field data frame to June observations.
df_field_Jun=df_field.query('date == "2009-06-22"')

# Merge June data frames.
df_Jun=pd.merge(df_Jun_22,df_field_Jun,on='MonitoringLocationIdentifier',how='outer')

# Subset field data frame to July observations.
df_field_Jul=df_field.query('date == "2009-07-06"')

# Merge July data frames.
df_Jul=pd.merge(df_Jul_08,df_field_Jul,on='MonitoringLocationIdentifier',how='outer')

# Subset field data frame to August observations.
df_field_Aug=df_field.query('date == "2009-08-03"')

# Merge August data frames.
df_Aug=pd.merge(df_Aug_09,df_field_Aug,on='MonitoringLocationIdentifier',how='outer')

# Subset field data frame to September observations.
df_field_Sept=df_field.query('date == "2009-09-17"')

# Merge September data frames.
df_Sept=pd.merge(df_Sept_10,df_field_Sept,on='MonitoringLocationIdentifier',how='outer')

# Merge data frames for all months into a single data frame.
df_full=pd.concat([df_Jun,df_Jul,df_Aug,df_Sept],ignore_index=True)

# Check df_full column names.
print df_full.columns

# Drop unneeded columns.
df_full.drop(['MonitoringLocationIdentifier','Band_9','date'],axis=1,inplace=True)

# Drop observation full of missing values.
df_full.drop(df_full.index[[1]],axis=0,inplace=True)
df_full.reset_index(inplace=True,drop=True) # Reset row index.

# Give the full data frame more meaningful band names.
print df_full.columns
df_full.columns=['Blue','Green','Red','NIR','SWIR1','Thermal_L','Thermal_H','SWIR2','temp_C','chl_ugL','susp_mgL'] # Thermal_L: 'low' gain (less sensitive, use when surface brightness is high). Thermal_H: 'high' gain (more sensitive, use when surface brightness is low).

# Rearrange columns.
print df_full.columns
df_full=df_full[['susp_mgL','chl_ugL','temp_C','Blue','Green','Red','NIR','SWIR1','Thermal_L','Thermal_H','SWIR2']]

# Pickle it!
df_full.to_pickle('df_full.pkl')

# Now restart the kernel to clear up the workspace.

############################################################################
### Transform Variables Prior to Linear Regression & Remove Collinearity ###
############################################################################

# Import modules.
import os
import pandas as pd
import numpy as np

# Set working directory.
os.chdir(r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Code')

# Import remote sensing water quality module.
import rs_wq

# Load full data frame.
df_full=pd.read_pickle('df_full.pkl')

# Create new data frame for transformed data.
df_trans=df_full.astype(float)

# Add two indices to the data frame.

# Check column names.
df_trans.columns

# NDVI.
df_trans['NDVI']=(df_trans['NIR'] - df_trans['Red']) / (df_trans['NIR'] + df_trans['Red'])

# SAVI (L = 0.5).
df_trans['SAVI']=(1.5 * (df_trans['NIR'] - df_trans['Red'])) / (df_trans['NIR'] + df_trans['Red'] + 0.5)

# Check distributions of variables.
rs_wq.normplot(df=df_trans)

# Need to transform the responses to achieve approximate normality and some predictors to reduce influential points.
    # Variable: transformation
    # susp_mgL: natural log
    # chl_ugL: natural log
    # NIR: natural log
    # SWIR1: natural log
    # Thermal_L: raise to power of 10
    # Thermal_H: raise to power of 10
    # SWIR2: natural log
    # NDVI: natural log
    # SAVI: natural log

# Transform susp_mgL (log).
df_trans['susp_mgL']=np.log(df_trans['susp_mgL'])
df_trans.rename(columns={'susp_mgL':'log_susp_mgL'},inplace=True) # Rename single column.

# Transform chl_ugL (log).
df_trans['chl_ugL']=np.log(df_trans['chl_ugL'])
df_trans.rename(columns={'chl_ugL':'log_chl_ugL'},inplace=True) # Rename single column.

# Transform NIR (log).
df_trans['NIR']=np.log(df_trans['NIR'])
df_trans.rename(columns={'NIR':'log_NIR'},inplace=True) # Rename single column.

# Transfrom SWIR1 (log).
df_trans['SWIR1']=np.log(df_trans['SWIR1'])
df_trans.rename(columns={'SWIR1':'log_SWIR1'},inplace=True) # Rename single column.

# Transform Thermal_L (^10).
df_trans['Thermal_L']=df_trans['Thermal_L']**10
df_trans.rename(columns={'Thermal_L':'Thermal_L^10'},inplace=True) # Rename single column.

# Transform Thermal_H (^10).
df_trans['Thermal_H']=df_trans['Thermal_H']**10
df_trans.rename(columns={'Thermal_H':'Thermal_H^10'},inplace=True) # Rename single column.

# Transform SWIR2 (log).
df_trans['SWIR2']=np.log(df_trans['SWIR2'])
df_trans.rename(columns={'SWIR2':'log_SWIR2'},inplace=True) # Rename single column.

# Transform NDVI (log). Add 1.1 to make values positive for log to work.
df_trans['NDVI']=np.log(1.1 + df_trans['NDVI'])
df_trans.rename(columns={'NDVI':'log_NDVI'},inplace=True) # Rename single column.

# Transform SAVI (log). Add 1.1 to make values positive for log to work.
df_trans['SAVI']=np.log(1.1 + df_trans['SAVI'])
df_trans.rename(columns={'SAVI':'log_SAVI'},inplace=True) # Rename single column.

# Linear regression doesn't like the huge values stored in the transformed thermal bands.

# Divide Thermal_L^10 by 10^18 to make the values small enough for linear regression.
df_trans['Thermal_L^10']=df_trans['Thermal_L^10']/(10**18)
df_trans.rename(columns={'Thermal_L^10':'(Thermal_L^10)/10'},inplace=True) # Rename single column.

# Divide Thermal_H^10 by 10^18 to make the values small enough for linear regression.
df_trans['Thermal_H^10']=df_trans['Thermal_H^10']/(10**18)
df_trans.rename(columns={'Thermal_H^10':'(Thermal_H^10)/10'},inplace=True) # Rename single column.

# Plot transformed variables.
rs_wq.normplot(df=df_trans)

# Need to remove highly collinear variables for linear regression.
corr=rs_wq.corr(df=df_trans,exclude=['log_susp_mgL','log_chl_ugL','temp_C'])
print corr

# There's issues with highly collinear variables.

# Blue, Green, and Red are highly collinear.
   # Remove Blue and Red.
   # Keep Green.
   # Rational: MSS does not have Blue, and the bands remaining after dealing with collinearity should be as different from each other as possible.

# log_NIR, log_SWIR1, and log_SWIR2 are highly collinear.
   # Remove log_SWIR1 and log_SWIR2.
   # Keep log_NIR.
   # Rational: MSS does not have SWIR1 nor SWIR2.

# (Thermal_L^10)/10 and (Thermal_H^10)/10 are highly collinear.
   # Remove (Thermal_L^10)/10.
   # Keep (Thermal_H^10)/10.
   # Rational: The high gain band is more sensitive.

# log_NDVI and log_SAVI are highly collinear.
   # Remove log_SAVI.
   # Keep log_NDVI.
   # Rational: How reliable is SAVI in water?

# Keep only the response variables and Green, log_NIR, (Thermal_H^10)/10, and log_NDVI.
df_trans=df_trans[['log_susp_mgL','log_chl_ugL','temp_C','Green','log_NIR','(Thermal_H^10)/10','log_NDVI']]

# Get correlation matrix.
corr=rs_wq.corr(df=df_trans,exclude=['log_susp_mgL','log_chl_ugL','temp_C'])
print corr

# Review plots of variables. Ready to go!
rs_wq.normplot(df_trans)

# Pickle the transformed data frame.
df_trans.to_pickle('df_trans.pkl')

# Now restart the kernel to clear up the workspace.

##############################################################
### Fit Linear Regression Model for Total Suspended Solids ###
##############################################################

# Import modules.
import os
import pandas as pd

# Set working directory.
os.chdir(r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Code')

# Import remote sensing water quality module.
import rs_wq

# Load transformed data frame.
df_trans=pd.read_pickle('df_trans.pkl')

# Remove log_chl_ugL and temp_C to get a data frame for regression on log_susp_mgL.
df_trans.columns
df_susp=df_trans.drop(['log_chl_ugL','temp_C'],1)

# Create scatter plots
rs_wq.scatterplot(df=df_susp,y='log_susp_mgL') # The thermal and NDVI bands visually appear to have the strongest correlations with total suspended solids.

# Fit a full linear regression model for log_susp_mgL.
rs_wq.lm_full(df=df_susp,y='log_susp_mgL')
    # Adjusted R-Squared: 0.210
    # AIC: 50.0

# Fit a full linear regression model without the thermal band (MSS doesn't have a thermal band).
rs_wq.lm_full(df=df_susp.drop('(Thermal_H^10)/10',1),y='log_susp_mgL')
    # Adjusted R-Squared: 0.159
    # AIC: 51.1

# Use backward variable selection for log_susp_mgL.
rs_wq.lm_backward(df=df_susp,y='log_susp_mgL',p_value_to_include=0.15)
    # Variables:
        # (Thermal_H^10)/10
    # Adjusted R-Squared: 0.127
    # AIC: 50.5

# Use backward variable selection for log_susp_mgL without the thermal band.
rs_wq.lm_backward(df=df_susp.drop('(Thermal_H^10)/10',1),y='log_susp_mgL',p_value_to_include=0.15)
    # Variables:
        # Green
        # log_NIR
    # Adjusted R-Squared: 0.137
    # AIC: 51.0

# Use forward variable selection for log_susp_mgL.
rs_wq.lm_forward(df=df_susp,y='log_susp_mgL',p_value_to_include=0.15)
    # Variables:
        # (Thermal_H^10)/10
        # log_NDVI
    # Adjusted R-Squared: 0.168
    # AIC: 49.9

# Use forward variable selection for log_susp_mgL without the thermal band.
rs_wq.lm_forward(df=df_susp.drop('(Thermal_H^10)/10',1),y='log_susp_mgL',p_value_to_include=0.15)
    # Variables:
        # log_NDVI
    # Adjusted R-Squared: 0.111
    # AIC: 51.0

# Use stepwise variable selection for log_susp_mgL.
rs_wq.lm_step(df=df_susp,y='log_susp_mgL',p_value_to_include=0.15)
    # Variables:
        # (Thermal_H^10)/10
        # log_NDVI
    # Adjusted R-Squared: 0.168
    # AIC: 49.9

# Use stepwise variable selection for log_susp_mgL without the thermal band.
rs_wq.lm_step(df=df_susp.drop('(Thermal_H^10)/10',1),y='log_susp_mgL',p_value_to_include=0.15)
    # Variables:
        # log_NDVI
    # Adjusted R-Squared: 0.111
    # AIC: 51.0

### - Final linear model for total suspended solids - ###
## Fit a full linear regression model for log_susp_mgL. ##
rs_wq.lm_full(df=df_susp,y='log_susp_mgL')
    # Parameter Estimates:
        # Intercept: -4.055282
        # Green: -0.058964
        # log_NIR: 3.295627
        # (Thermal_H^10)/10: -0.000118
        # log_NDVI: -4.882161
    # P-Values:
        # Intercept: 0.404614
        # Green: 0.114489
        # log_NIR: 0.087922
        # (Thermal_H^10)/10: 0.109922
        # log_NDVI: 0.184416
    # Adjusted R-Squared: 0.210
    # AIC: 50.0

## Rational: This model has the highest adjusted R-squared by quite a bit. Although it doesn't have the lowest AIC, its AIC is very close to matching the lowest. ##

# Now restart the kernel to clear up the workspace.

#####################################################
### Fit Linear Regression Model for Chlorophyll a ###
#####################################################

# Import modules.
import os
import pandas as pd

# Set working directory.
os.chdir(r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Code')

# Import remote sensing water quality module.
import rs_wq

# Load transformed data frame.
df_trans=pd.read_pickle('df_trans.pkl')

# Remove log_susp_mgL and temp_C to get a data frame for regression on log_chl_ugL.
df_trans.columns
df_chl=df_trans.drop(['log_susp_mgL','temp_C'],1)

# Create scatter plots
rs_wq.scatterplot(df=df_chl,y='log_chl_ugL') # The Green and NDVI bands visually appear to have the strongest correlations with chlorophyll a.

# Fit a full linear regression model for log_chl_ugL.
rs_wq.lm_full(df=df_chl,y='log_chl_ugL')
    # Adjusted R-Squared: 0.325
    # AIC: 85.5

# Fit a full linear regression model without the thermal band (MSS doesn't have a thermal band).
rs_wq.lm_full(df=df_chl.drop('(Thermal_H^10)/10',1),y='log_chl_ugL')
    # Adjusted R-Squared: 0.330
    # AIC: 84.4

# Use backward variable selection for log_chl_ugL.
rs_wq.lm_backward(df=df_chl,y='log_chl_ugL',p_value_to_include=0.15)
    # Variables:
        # Green
        # log_NIR
    # Adjusted R-Squared: 0.344
    # AIC: 82.9

# Use backward variable selection for log_chl_ugL without the thermal band.
rs_wq.lm_backward(df=df_chl.drop('(Thermal_H^10)/10',1),y='log_chl_ugL',p_value_to_include=0.15)
    # Variables:
        # Green
        # log_NIR
    # Adjusted R-Squared: 0.344
    # AIC: 82.9

# Use forward variable selection for log_chl_ugL.
rs_wq.lm_forward(df=df_chl,y='log_chl_ugL',p_value_to_include=0.15)
    # Variables:
        # log_NDVI
        # Green
    # Adjusted R-Squared: 0.323
    # AIC: 83.9

# Use forward variable selection for log_chl_ugL without the thermal band.
rs_wq.lm_forward(df=df_chl.drop('(Thermal_H^10)/10',1),y='log_chl_ugL',p_value_to_include=0.15)
    # Variables:
        # log_NDVI
        # Green
    # Adjusted R-Squared: 0.323
    # AIC: 83.9

# Use stepwise variable selection for log_chl_ugL.
rs_wq.lm_step(df=df_chl,y='log_chl_ugL',p_value_to_include=0.15)
    # Variables:
        # log_NDVI
        # Green
    # Adjusted R-Squared: 0.323
    # AIC: 83.9

# Use stepwise variable selection for log_chl_ugL without the thermal band.
rs_wq.lm_step(df=df_chl.drop('(Thermal_H^10)/10',1),y='log_chl_ugL',p_value_to_include=0.15)
    # Variables:
        # log_NDVI
        # Green
    # Adjusted R-Squared: 0.323
    # AIC: 83.9

### - Final linear model for chlorophyll a - ###
## Use backward variable selection for log_chl_ugL. ##
rs_wq.lm_backward(df=df_chl,y='log_chl_ugL',p_value_to_include=0.15)
    # Parameter Estimates:
        # Intercept: 1.328964
        # Green: -0.050018
        # log_NIR: 1.696873
    # P-Values:
        # Intercept: 0.461206
        # Green: 0.000238
        # log_NIR: 0.018840
    # Adjusted R-Squared: 0.344
    # AIC: 82.9

## Rational: This model has the highest adjusted R-squared and lowest AIC. ##

# Now restart the kernel to clear up the workspace.

###################################################
### Fit Linear Regression Model for Temperature ###
###################################################

# Import modules.
import os
import pandas as pd

# Set working directory.
os.chdir(r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Code')

# Import remote sensing water quality module.
import rs_wq

# Load transformed data frame.
df_trans=pd.read_pickle('df_trans.pkl')

# Remove log_susp_mgL and log_chl_ugL to get a data frame for regression on temp_C.
df_trans.columns
df_temp=df_trans.drop(['log_susp_mgL','log_chl_ugL'],1)
df_temp=df_temp[pd.notnull(df_temp['temp_C'])] # Remove observations with missing values.

# Create scatter plots
rs_wq.scatterplot(df=df_temp,y='temp_C') # All bands visually appear to have similarly poor correlations with temperature.

# Fit a full linear regression model for temp_C.
rs_wq.lm_full(df=df_temp,y='temp_C')
    # Adjusted R-Squared: 0.144
    # AIC: 119.7

# Fit a full linear regression model without the thermal band (MSS doesn't have a thermal band).
rs_wq.lm_full(df=df_temp.drop('(Thermal_H^10)/10',1),y='temp_C')
    # Adjusted R-Squared: 0.100
    # AIC: 120.3

# Use backward variable selection for temp_C.
rs_wq.lm_backward(df=df_temp,y='temp_C',p_value_to_include=0.15)
    # Variables:
        # Green
        # log_NIR
        # log_NDVI
    # Adjusted R-Squared: 0.100
    # AIC: 120.3

# Use backward variable selection for temp_C without the thermal band.
rs_wq.lm_backward(df=df_temp.drop('(Thermal_H^10)/10',1),y='temp_C',p_value_to_include=0.15)
    # Variables:
        # Green
        # log_NIR
        # log_NDVI
    # Adjusted R-Squared: 0.100
    # AIC: 120.3

# Use forward variable selection for temp_C.
rs_wq.lm_forward(df=df_temp,y='temp_C',p_value_to_include=0.15)
    # Variables:
        # log_NIR
    # Adjusted R-Squared: 0.051
    # AIC: 119.8

# Use forward variable selection for temp_C without the thermal band.
rs_wq.lm_forward(df=df_temp.drop('(Thermal_H^10)/10',1),y='temp_C',p_value_to_include=0.15)
    # Variables:
        # log_NIR
    # Adjusted R-Squared: 0.051
    # AIC: 119.8

# Use stepwise variable selection for temp_C.
rs_wq.lm_step(df=df_temp,y='temp_C',p_value_to_include=0.15)
    # Variables:
        # log_NIR
    # Adjusted R-Squared: 0.051
    # AIC: 119.8

# Use stepwise variable selection for temp_C without the thermal band.
rs_wq.lm_step(df=df_temp.drop('(Thermal_H^10)/10',1),y='temp_C',p_value_to_include=0.15)
    # Variables:
        # log_NIR
    # Adjusted R-Squared: 0.051
    # AIC: 119.8

### - Final linear model for temperature - ###
## Fit a full linear regression model for temp_C. ##
rs_wq.lm_full(df=df_temp,y='temp_C')
    # Paramter Estimates:
        # Intercept: -30.876263
        # Green: -0.334799
        # log_NIR: 19.575596
        # (Thermal_H^10)/10: 0.000508
        # log_NDVI: -34.155291
    # P-Values:
        # Intercept: 0.218119
        # Green: 0.096559
        # log_NIR: 0.053074
        # (Thermal_H^10)/10: 0.162959
        # log_NDVI: 0.088390
    # Adjusted R-Squared: 0.144
    # AIC: 119.7

## Rational: This model has the highest adjusted R-squared and lowest AIC. ##

# Now restart the kernel to clear up the workspace.

##############################
### Review of Final Models ###
##############################

# Import modules.
import os
import pandas as pd

# Set working directory.
os.chdir(r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Code')

# Import remote sensing water quality module.
import rs_wq

# Load transformed data frame.
df_trans=pd.read_pickle('df_trans.pkl')

# Get a separate data frame for each response.
df_trans.columns
df_susp=df_trans.drop(['log_chl_ugL','temp_C'],1) # Data frame for log_susp_mgL.
df_chl=df_trans.drop(['log_susp_mgL','temp_C'],1) # Data frame for log_chl_ugL.
df_temp=df_trans.drop(['log_susp_mgL','log_chl_ugL'],1) # Data frame for temp_C.
df_temp=df_temp[pd.notnull(df_temp['temp_C'])] # Remove observations with missing values from data frame for temp_C.

# Final linear model for total suspended solids.
rs_wq.lm_full(df=df_susp,y='log_susp_mgL') # Full linear regression model.
    # R-Squared: 0.315
    # Adjusted R-Squared: 0.210
    # AIC: 50.0
    # Parameter Estimates:
        # Intercept: -4.055282
        # Green: -0.058964
        # log_NIR: 3.295627
        # (Thermal_H^10)/10: -0.000118
        # log_NDVI: -4.882161
    # P-Values:
        # Intercept: 0.405
        # Green: 0.114
        # log_NIR: 0.088
        # (Thermal_H^10)/10: 0.110
        # log_NDVI: 0.184
    # Rational: This model has the highest adjusted R-squared by quite a bit. Although it doesn't have the lowest AIC, its AIC is very close to matching the lowest.

# Final linear model for chlorophyll a.
rs_wq.lm_backward(df=df_chl,y='log_chl_ugL',p_value_to_include=0.15) # Backward selection linear regression model.
    # R-Squared: 0.388
    # Adjusted R-Squared: 0.344
    # AIC: 82.9
    # Parameter Estimates:
        # Intercept: 1.328964
        # Green: -0.050018
        # log_NIR: 1.696873
    # P-Values:
        # Intercept: 0.461
        # Green: < 0.001
        # log_NIR: 0.019
    # Rational: This model has the highest adjusted R-squared and lowest AIC.

# Final linear model for temperature.
rs_wq.lm_full(df=df_temp,y='temp_C') # Full linear regression model.
    # R-Squared: 0.287
    # Adjusted R-Squared: 0.144
    # AIC: 119.7
    # Paramter Estimates:
        # Intercept: -30.876263
        # Green: -0.334799
        # log_NIR: 19.575596
        # (Thermal_H^10)/10: 0.000508
        # log_NDVI: -34.155291
    # P-Values:
        # Intercept: 0.218
        # Green: 0.097
        # log_NIR: 0.053
        # (Thermal_H^10)/10: 0.163
        # log_NDVI: 0.088
    # Rational: This model has the highest adjusted R-squared and lowest AIC.

# Since only the chlorophyll a model does not use the thermal band, chlorophyll a will be the only water quality parameter to be predicted for the MSS Landsat images.

# These bands will be needed:
    # MSS images: Green, NIR
    # All images except MSS: Green, NIR, Thermal (high gain), Red (for NDVI calculation)

# Now restart the kernel to clear up the workspace.

##################################################
### Add an NDVI Band to all of the Time Images ###
##################################################

# Import modules.
import os

# Set working directory.
os.chdir(r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Code')

# Import remote sensing water quality module.
import rs_wq

# Add an NDVI band to the Landsat MSS images.
rs_wq.add_ndvi(inputdirectory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Bulk Order 960324\Landsat 1-5 MSS C1 Level-1\Mosaics', outputdirectory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Plus_NDVI\Landsat 1-5 MSS C1 Level-1', red_band_num=2, nir_band_num=4)

# Add an NDVI band to the Landsat TM images.
rs_wq.add_ndvi(inputdirectory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Bulk Order 960324\Landsat 4-5 TM C1 Level-1\Mosaics', outputdirectory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Plus_NDVI\Landsat 4-5 TM C1 Level-1', red_band_num=3, nir_band_num=4)

# Add an NDVI band to the Landsat ETM+ images.
rs_wq.add_ndvi(inputdirectory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Bulk Order 960324\Landsat 7 ETM_ C1 Level-1\Mosaics', outputdirectory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Plus_NDVI\Landsat 7 ETM_ C1 Level-1', red_band_num=3, nir_band_num=4)

# Add an NDVI band to the Landsat OLI & TIRS image.
rs_wq.add_ndvi(inputdirectory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Bulk Order 960324\Landsat 8 OLI_TIRS C1 Level-1\Mosaics', outputdirectory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Plus_NDVI\Landsat 8 OLI_TIRS C1 Level-1', red_band_num=6, nir_band_num=7)

##################################################################
### Apply Linear Model for Total Suspended Solids Across Years ###
##################################################################

# Import modules.
import os
import numpy as np
import glob
import matplotlib.pyplot as plt

# Set working directory.
os.chdir(r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Code')

# Import remote sensing water quality module.
import rs_wq

# Linear model for total suspended solids (log transformed).
    # Parameter Estimates:
        # Intercept: -4.055282
        # Green: -0.058964
            # LS TM Band 2 --> Raster Band 2
            # LS ETM+ Band 2 --> Raster Band 2
            # LS OLI & TIRS Band 3 --> Raster Band 5
        # log_NIR: 3.295627
            # LS TM Band 4 --> Raster Band 4
            # LS ETM+ Band 4 --> Raster Band 4
            # LS OLI & TIRS Band 5 --> Raster Band 7
        # (Thermal_H^10)/10: -0.000118
            # LS TM Band 6 --> Raster Band 6
            # LS ETM+ Band 6 (62 or 6H) --> Raster Band 7
            # LS OLI & TIRS Band 10 --> Raster Band 3
        # log_NDVI: -4.882161
            # LS TM Raster Band 9
            # LS ETM+ Raster Band 10
            # LS OLI & TIRS Raster Band 12

# Predict across years for total suspended solids.

# Start with Landsat TM images.
# Set parameters for apply linear model function.
directory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Plus_NDVI\Landsat 4-5 TM C1 Level-1'
predictor_band_numbers=[2, 4, 6, 9]
parameter_estimates=[-0.058964,3.295627,-0.000118,-4.882161]
intercept=-4.055282
predictor_transformations=['None','np.log(x)','((x**10)/(10**18))','np.log(1.1+x)']
response_backtransformation='np.exp(y)'
mask_by_NoData=0
mask_by_cloud=8

# Loop through all TIF files within the Landsat TM directory and apply the linear model.
array_list=[] # Create an empty list to store NumPy arrays of predicted values.
for i in glob.glob(directory+"/*.TIF"): # Loop through TIF files in directory.
    y=rs_wq.apply_lm(raster=i,predictor_band_numbers=predictor_band_numbers,parameter_estimates=parameter_estimates,intercept=intercept,predictor_transformations=predictor_transformations,response_backtransformation=response_backtransformation,mask_by_NoData=mask_by_NoData,mask_by_cloud=mask_by_cloud) # Apply linear model.
    array_list.append(y) # Append prediction array to the list of NumPy arrays.

# Next with Landsat ETM+ images.
# Set parameters for apply linear model function.
directory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Plus_NDVI\Landsat 7 ETM_ C1 Level-1'
predictor_band_numbers=[2, 4, 7, 10]
parameter_estimates=[-0.058964,3.295627,-0.000118,-4.882161]
intercept=-4.055282
predictor_transformations=['None','np.log(x)','((x**10)/(10**18))','np.log(1.1+x)']
response_backtransformation='np.exp(y)'
mask_by_NoData=0
mask_by_cloud=9

# Loop through all TIF files within the Landsat ETM+ directory and apply the linear model.
for i in glob.glob(directory+"/*.TIF"): # Loop through TIF files in directory.
    y=rs_wq.apply_lm(raster=i,predictor_band_numbers=predictor_band_numbers,parameter_estimates=parameter_estimates,intercept=intercept,predictor_transformations=predictor_transformations,response_backtransformation=response_backtransformation,mask_by_NoData=mask_by_NoData,mask_by_cloud=mask_by_cloud) # Apply linear model.
    array_list.append(y) # Append prediction array to the list of NumPy arrays.

# Skipping Landsat OLI & TIRS images because the thermal band has been split into two different wavelength ranges.
# Neither of the two OLI & TIRS thermal bands can be used without creating issues with overflow values when the thermal band is transformed.

# Plot it!
name_list=['1991','1995','2000','2005','2010'] # Create a list of image years.

# Map.
plt.figure(figsize=(28,20)) # Create new figure (28 in. wide by 20 in. heigh).
plt.rcParams.update({'font.size': 32}) # Change the font size.
plt.suptitle('Total Suspended Solids (mg/L)') # Add super title.
for j in range(len(array_list)): # Loop through the NumPy arrays stored in the list of arrays.
    plt.subplot(2, 3, j + 1) # Create a subplot.
    lower_pct=np.percentile(array_list[j].compressed(),5) # Apply a percentile stretch with a lower percentile of 5%...
    upper_pct=np.percentile(array_list[j].compressed(),95) # ...and an upper percentile of 95%.
    plt.imshow(array_list[j], norm=plt.Normalize(lower_pct, upper_pct), cmap='coolwarm') # Create a map.
    plt.axis('off') # Turn axis off.
    plt.colorbar() # Add a colorbar legend.
    plt.title(name_list[j]) # Add a name to the map.
plt.savefig('susp_map.png', transparent=False) # Save the figure as a PNG without a transparent background.

# Boxplot.
array_list_flattened=[] # Create new empty list to store flattened array_list.
for j in array_list: # Loop through array_list.
    array_compressed=j.compressed() # Compress NumPy array to remove masked values.
    array_list_flattened.append(array_compressed.flatten()) # Flatten NumPy array.
plt.figure(figsize=(28,20)) # Create new figure (28 in. wide by 20 in. heigh).
plt.rcParams.update({'font.size': 32}) # Change the font size.
plt.boxplot(array_list_flattened,showfliers=False,showmeans=True) # Create boxplot.
plt.xlabel('Year') # Label x-axis.
name_numbers=np.array(range(len(name_list)))+1 # Get an array of x-axis tick positions.
plt.xticks(name_numbers.tolist(),name_list) # Label x-axis ticks.
plt.title('Total Suspended Solids (mg/L)') # Add a plot title.
plt.savefig('susp_boxplot.png', transparent=False) # Save the figure as a PNG without a transparent background.

# Restart the kernel.

#########################################################
### Apply Linear Model for Chlorophyll a Across Years ###
#########################################################

# Import modules.
import os
import numpy as np
import glob
import matplotlib.pyplot as plt

# Set working directory.
os.chdir(r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Code')

# Import remote sensing water quality module.
import rs_wq

# Linear model for chlorophyll a (log transformed).
    # Parameter Estimates:
        # Intercept: 1.328964
        # Green: -0.050018
            # LS MSS Band 4 --> Raster Band 1
            # LS TM Band 2 --> Raster Band 2
            # LS ETM+ Band 2 --> Raster Band 2
            # LS OLI & TIRS Band 3 --> Raster Band 5
        # log_NIR: 1.696873
            # LS MSS Band 7 --> Raster Band 4
            # LS TM Band 4 --> Raster Band 4
            # LS ETM+ Band 4 --> Raster Band 4
            # LS OLI & TIRS Band 5 --> Raster Band 7

# Predict across years for chlorophyll a.

# Start with Landsat MSS images.
# Set parameters for apply linear model function.
directory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Plus_NDVI\Landsat 1-5 MSS C1 Level-1'
predictor_band_numbers=[1, 4]
parameter_estimates=[-0.050018, 1.696873]
intercept=1.328964
predictor_transformations=['None','np.log(x)']
response_backtransformation='np.exp(y)'
mask_by_NoData=0
mask_by_cloud=5

# Loop through all TIF files within the Landsat MSS directory and apply the linear model.
array_list=[] # Create an empty list to store NumPy arrays of predicted values.
for i in glob.glob(directory+"/*.TIF"): # Loop through TIF files in directory.
    y=rs_wq.apply_lm(raster=i,predictor_band_numbers=predictor_band_numbers,parameter_estimates=parameter_estimates,intercept=intercept,predictor_transformations=predictor_transformations,response_backtransformation=response_backtransformation,mask_by_NoData=mask_by_NoData,mask_by_cloud=mask_by_cloud) # Apply linear model.
    array_list.append(y) # Append prediction array to the list of NumPy arrays.

# Next with Landsat TM images.
# Set parameters for apply linear model function.
directory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Plus_NDVI\Landsat 4-5 TM C1 Level-1'
predictor_band_numbers=[2, 4]
parameter_estimates=[-0.050018, 1.696873]
intercept=1.328964
predictor_transformations=['None','np.log(x)']
response_backtransformation='np.exp(y)'
mask_by_NoData=0
mask_by_cloud=8

# Loop through all TIF files within the Landsat TM directory and apply the linear model.
for i in glob.glob(directory+"/*.TIF"): # Loop through TIF files in directory.
    y=rs_wq.apply_lm(raster=i,predictor_band_numbers=predictor_band_numbers,parameter_estimates=parameter_estimates,intercept=intercept,predictor_transformations=predictor_transformations,response_backtransformation=response_backtransformation,mask_by_NoData=mask_by_NoData,mask_by_cloud=mask_by_cloud) # Apply linear model.
    array_list.append(y) # Append prediction array to the list of NumPy arrays.

# Next with Landsat ETM+ images.
# Set parameters for apply linear model function.
directory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Plus_NDVI\Landsat 7 ETM_ C1 Level-1'
predictor_band_numbers=[2, 4]
parameter_estimates=[-0.050018, 1.696873]
intercept=1.328964
predictor_transformations=['None','np.log(x)']
response_backtransformation='np.exp(y)'
mask_by_NoData=0
mask_by_cloud=9

# Loop through all TIF files within the Landsat ETM+ directory and apply the linear model.
for i in glob.glob(directory+"/*.TIF"): # Loop through TIF files in directory.
    y=rs_wq.apply_lm(raster=i,predictor_band_numbers=predictor_band_numbers,parameter_estimates=parameter_estimates,intercept=intercept,predictor_transformations=predictor_transformations,response_backtransformation=response_backtransformation,mask_by_NoData=mask_by_NoData,mask_by_cloud=mask_by_cloud) # Apply linear model.
    array_list.append(y) # Append prediction array to the list of NumPy arrays.

# Skipping Landsat OLI & TIRS images because the NIR band has been substantially changed.
# Applying the linear model to OLI & TIRS appears to give especially unreliable results.

# Plot it!
name_list=['1974','1980','1985','1991','1995','2000','2005','2010'] # Create a list of image years.

# Map.
plt.figure(figsize=(28,20)) # Create new figure (28 in. wide by 20 in. heigh).
plt.rcParams.update({'font.size': 32}) # Change the font size.
plt.suptitle('Chlorophyll a (ug/L)') # Add super title.
for j in range(len(array_list)): # Loop through the NumPy arrays stored in the list of arrays.
    plt.subplot(3, 3, j + 1) # Create a subplot.
    lower_pct=np.percentile(array_list[j].compressed(),5) # Apply a percentile stretch with a lower percentile of 5%...
    upper_pct=np.percentile(array_list[j].compressed(),95) # ...and an upper percentile of 95%.
    plt.imshow(array_list[j], norm=plt.Normalize(lower_pct, upper_pct), cmap='coolwarm') # Create a map.
    plt.axis('off') # Turn axis off.
    plt.colorbar() # Add a colorbar legend.
    plt.title(name_list[j]) # Add a name to the map.
plt.savefig('chl_map.png', transparent=False) # Save the figure as a PNG without a transparent background.

# Boxplot.
array_list_flattened=[] # Create new empty list to store flattened array_list.
for j in array_list: # Loop through array_list.
    array_compressed=j.compressed() # Compress NumPy array to remove masked values.
    array_list_flattened.append(array_compressed.flatten()) # Flatten NumPy array.
plt.figure(figsize=(28,20)) # Create new figure (28 in. wide by 20 in. heigh).
plt.rcParams.update({'font.size': 32}) # Change the font size.
plt.boxplot(array_list_flattened,showfliers=False,showmeans=True) # Create boxplot.
plt.xlabel('Year') # Label x-axis.
name_numbers=np.array(range(len(name_list)))+1 # Get an array of x-axis tick positions.
plt.xticks(name_numbers.tolist(),name_list) # Label x-axis ticks.
plt.title('Chlorophyll a (ug/L)') # Add a plot title.
plt.savefig('chl_boxplot.png', transparent=False) # Save the figure as a PNG without a transparent background.

# Restart the kernel.

#######################################################
### Apply Linear Model for Temperature Across Years ###
#######################################################

# Import modules.
import os
import numpy as np
import glob
import matplotlib.pyplot as plt

# Set working directory.
os.chdir(r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Code')

# Import remote sensing water quality module.
import rs_wq

# Linear model for temperature (not transformed).
    # Paramter Estimates:
        # Intercept: -30.876263
        # Green: -0.334799
            # LS TM Band 2 --> Raster Band 2
            # LS ETM+ Band 2 --> Raster Band 2
        # log_NIR: 19.575596
            # LS TM Band 4 --> Raster Band 4
            # LS ETM+ Band 4 --> Raster Band 4
        # (Thermal_H^10)/10: 0.000508
            # LS TM Band 6 --> Raster Band 6
            # LS ETM+ Band 6 (62 or 6H) --> Raster Band 7
        # log_NDVI: -34.155291
            # LS TM Raster Band 9
            # LS ETM+ Raster Band 10

# Predict across years for temperature.

# Start with Landsat TM images.
# Set parameters for apply linear model function.
directory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Plus_NDVI\Landsat 4-5 TM C1 Level-1'
predictor_band_numbers=[2, 4, 6, 9]
parameter_estimates=[-0.334799, 19.575596, 0.000508, -34.155291]
intercept=-30.876263
predictor_transformations=['None','np.log(x)','((x**10)/(10**18))','np.log(1.1+x)']
response_backtransformation='None'
mask_by_NoData=0
mask_by_cloud=8

# Loop through all TIF files within the Landsat TM directory and apply the linear model.
array_list=[] # Create an empty list to store NumPy arrays of predicted values.
for i in glob.glob(directory+"/*.TIF"): # Loop through TIF files in directory.
    y=rs_wq.apply_lm(raster=i,predictor_band_numbers=predictor_band_numbers,parameter_estimates=parameter_estimates,intercept=intercept,predictor_transformations=predictor_transformations,response_backtransformation=response_backtransformation,mask_by_NoData=mask_by_NoData,mask_by_cloud=mask_by_cloud) # Apply linear model.
    array_list.append(y) # Append prediction array to the list of NumPy arrays.

# Next with Landsat ETM+ images.
# Set parameters for apply linear model function.
directory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Plus_NDVI\Landsat 7 ETM_ C1 Level-1'
predictor_band_numbers=[2, 4, 7, 10]
parameter_estimates=[-0.334799, 19.575596, 0.000508, -34.155291]
intercept=-30.876263
predictor_transformations=['None','np.log(x)','((x**10)/(10**18))','np.log(1.1+x)']
response_backtransformation='None'
mask_by_NoData=0
mask_by_cloud=9

# Loop through all TIF files within the Landsat ETM+ directory and apply the linear model.
for i in glob.glob(directory+"/*.TIF"): # Loop through TIF files in directory.
    y=rs_wq.apply_lm(raster=i,predictor_band_numbers=predictor_band_numbers,parameter_estimates=parameter_estimates,intercept=intercept,predictor_transformations=predictor_transformations,response_backtransformation=response_backtransformation,mask_by_NoData=mask_by_NoData,mask_by_cloud=mask_by_cloud) # Apply linear model.
    array_list.append(y) # Append prediction array to the list of NumPy arrays.

# Skipping Landsat OLI & TIRS images because the NIR and thermal bands have been substantially changed.

# Plot it!
name_list=['1991','1995','2000','2005','2010'] # Create a list of image years.

# Map.
plt.figure(figsize=(28,20)) # Create new figure (28 in. wide by 20 in. heigh).
plt.rcParams.update({'font.size': 32}) # Change the font size.
plt.suptitle('Water Temperature (C)') # Add super title.
for j in range(len(array_list)): # Loop through the NumPy arrays stored in the list of arrays.
    plt.subplot(2, 3, j + 1) # Create a subplot.
    lower_pct=np.percentile(array_list[j].compressed(),5) # Apply a percentile stretch with a lower percentile of 5%...
    upper_pct=np.percentile(array_list[j].compressed(),95) # ...and an upper percentile of 95%.
    plt.imshow(array_list[j], norm=plt.Normalize(lower_pct, upper_pct), cmap='coolwarm') # Create a map.
    plt.axis('off') # Turn axis off.
    plt.colorbar() # Add a colorbar legend.
    plt.title(name_list[j]) # Add a name to the map.
plt.savefig('temp_map.png', transparent=False) # Save the figure as a PNG without a transparent background.

# Boxplot.
array_list_flattened=[] # Create new empty list to store flattened array_list.
for j in array_list: # Loop through array_list.
    array_compressed=j.compressed() # Compress NumPy array to remove masked values.
    array_list_flattened.append(array_compressed.flatten()) # Flatten NumPy array.
plt.figure(figsize=(28,20)) # Create new figure (28 in. wide by 20 in. heigh).
plt.rcParams.update({'font.size': 32}) # Change the font size.
plt.boxplot(array_list_flattened,showfliers=False,showmeans=True) # Create boxplot.
plt.xlabel('Year') # Label x-axis.
name_numbers=np.array(range(len(name_list)))+1 # Get an array of x-axis tick positions.
plt.xticks(name_numbers.tolist(),name_list) # Label x-axis ticks.
plt.title('Water Temperature (C)') # Add a plot title.
plt.savefig('temp_boxplot.png', transparent=False) # Save the figure as a PNG without a transparent background.

# Restart the kernel.

##########################################
### Color Infrared Images of Utah Lake ###
##########################################

# Import modules.
import os
import numpy as np
import glob
import matplotlib.pyplot as plt

# Set working directory.
os.chdir(r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Code')

# Import remote sensing water quality module.
import rs_wq

# For MSS images.
    # NIR Band: 4
    # Red Band: 2
    # Green Band: 1

# Set MSS parameters.
directory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Plus_NDVI\Landsat 1-5 MSS C1 Level-1'
rgb=[4,2,1]
NoData_value=0
n=2

# Loop through all TIF files within the Landsat MSS directory and apply a standard deviation stretch.
array_list=[] # Create an empty list to store stacked NumPy arrays of raster images.
for raster in glob.glob(directory+"/*.TIF"): # Loop through TIF files in directory.
    stretched_bands=rs_wq.apply_std_stretch(raster=raster,rgb=rgb,n=n,NoData_value=NoData_value) # Apply a standard deviation stretch.
    array_list.append(stretched_bands) # Append stacked arrays to the list of NumPy arrays.

# For TM images.
    # NIR Band: 4
    # Red Band: 3
    # Green Band: 2

# Set TM parameters.
directory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Plus_NDVI\Landsat 4-5 TM C1 Level-1'
rgb=[4,3,2]

# Loop through all TIF files within the Landsat TM directory and apply a standard deviation stretch.
for raster in glob.glob(directory+"/*.TIF"): # Loop through TIF files in directory.
    stretched_bands=rs_wq.apply_std_stretch(raster=raster,rgb=rgb,n=n,NoData_value=NoData_value) # Apply a standard deviation stretch.
    array_list.append(stretched_bands) # Append stacked arrays to the list of NumPy arrays.

# For ETM+ images.
    # NIR Band: 4
    # Red Band: 3
    # Green Band: 2

# Set ETM+ parameters.
directory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Plus_NDVI\Landsat 7 ETM_ C1 Level-1'
rgb=[4,3,2]

# Loop through all TIF files within the Landsat ETM+ directory and apply a standard deviation stretch.
for raster in glob.glob(directory+"/*.TIF"): # Loop through TIF files in directory.
    stretched_bands=rs_wq.apply_std_stretch(raster=raster,rgb=rgb,n=n,NoData_value=NoData_value) # Apply a standard deviation stretch.
    array_list.append(stretched_bands) # Append stacked arrays to the list of NumPy arrays.

# For OLI & TIRS images.
    # NIR Band: 7
    # Red Band: 6
    # Green Band: 5

# Set OLI & TIRS parameters.
directory=r'C:\Users\Kenen\Desktop\Fall_2018\GIS_Project\Images\Time\Plus_NDVI\Landsat 8 OLI_TIRS C1 Level-1'
rgb=[7,6,5]

# Loop through all TIF files within the Landsat OLI & TIRS directory and apply a standard deviation stretch.
for raster in glob.glob(directory+"/*.TIF"): # Loop through TIF files in directory.
    stretched_bands=rs_wq.apply_std_stretch(raster=raster,rgb=rgb,n=n,NoData_value=NoData_value) # Apply a standard deviation stretch.
    array_list.append(stretched_bands) # Append stacked arrays to the list of NumPy arrays.

# Plot it!
name_list=['27 Jul. 1974','1 Aug. 1980','14 Jul. 1985','15 Jul. 1991','26 Jul. 1995','31 Jul. 2000','13 Jul. 2005','27 Jul. 2010','2 Aug. 2015'] # Create a list of image names.

# Map.
plt.figure(figsize=(28,20)) # Create new figure (28 in. wide by 20 in. heigh).
plt.rcParams.update({'font.size': 32}) # Change the font size.
plt.suptitle('Color Infrared Landsat Images') # Add super title.
for j in range(len(array_list)): # Loop through the NumPy arrays stored in the list of arrays.
    plt.subplot(3, 3, j + 1) # Create a subplot.
    plt.imshow(array_list[j]) # Create a map.
    plt.axis('off') # Turn axis off.
    plt.title(name_list[j]) # Add a name to the map.
plt.savefig('LS_images.png', transparent=False) # Save the figure as a PNG without a transparent background.

# Done!
