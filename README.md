# Remote_Sensing-Utah_Lake_Water_Quality

The rs_wq.py file contains a module with tools for remotely sensing total suspended solids, chlorophyll a, and water temperature. These tools fit models relating water quality field measurements to spectral responses which can be applied to predict water quality in new images.
The UTL_Code.py file contains script which uses this module to remotely sense water quality on Utah Lake, UT. See the [Wiki page](https://gitlab.com/Kenen_Goodwin/remote_sensing-utah_lake_water_quality/wikis/home) for more information.
See this [Google Slides presentation](https://docs.google.com/presentation/d/1NP6RhgW32rGWDcp0aiEEi-msyMD95Lc7KKzXiNI2WK4/edit?usp=sharing) for a brief presentation on how this module can be used.
The Presentation folder contains a Presentation_Code.py file which simplifies the overall process taken in the UTL_Code.py file using three images and fitting and applying a model for chlorophyll a.
All of the data used in the Presentation_Code.py file is provided in the Presentation folder. To try out the Presentation_Code.py file, download the Presentation folder and update the directories in the Presentation_Code.py file.