#########################
### PRESENTATION CODE ###
#########################

# Import modules.
import os
import pandas as pd
import glob
import matplotlib.pyplot as plt
import numpy as np

# Set working directory.
os.chdir(r'D:\Fall_2018\GIS_Project\Presentation\Code')

# Import remote sensing water quality module.
import rs_wq

# Create a mosaic raster file for each downloaded Landsat ETM+ image. The raster is stored in 16-bit unsigned to preserve the quality assessment band. The mosaic images are clipped to Utah Lake.
rs_wq.to_mosaic(inputdirectory=r'D:\Fall_2018\GIS_Project\Presentation\Images\Unzipped',outputdirectory=r'D:\Fall_2018\GIS_Project\Presentation\Images\Mosaic',sixteen_bit=True,shapefile_to_clip_to='UTL_Proper_Coordinates.shp')

# Extract values at points for the 2000 mosaic image. Set no data value to 0. Get average of a 3x3 window. Mask out clouded pixels.
df_2000=rs_wq.extract_values_at_points(raster=r'D:\Fall_2018\GIS_Project\Presentation\Images\Mosaic\LE07_L1TP_038032_20000731_20161002_01_T1.tif',points_shapefile='sample_pts_proper_projection.shp',point_identifier='Monitoring',mask_by_cloud=True,mask_by_NoData=0,window_size=3)

# Load transformed data frame.
df_trans=pd.read_pickle('df_trans.pkl')

# Check distributions of variables.
rs_wq.normplot(df=df_trans)

# Get correlation matrix. Exclude response variables.
corr=rs_wq.corr(df=df_trans,exclude=['log_susp_mgL','log_chl_ugL','temp_C'])

# Remove log_susp_mgL and temp_C to get a data frame for regression on log_chl_ugL.
df_chl=df_trans.drop(['log_susp_mgL','temp_C'],1)

# Create scatter plots
rs_wq.scatterplot(df=df_chl,y='log_chl_ugL')

# Use backward variable selection for log_chl_ugL.
rs_wq.lm_backward(df=df_chl,y='log_chl_ugL',p_value_to_include=0.15)

# Add an NDVI band to the Landsat ETM+ images.
rs_wq.add_ndvi(inputdirectory=r'D:\Fall_2018\GIS_Project\Presentation\Images\Mosaic', outputdirectory=r'D:\Fall_2018\GIS_Project\Presentation\Images\Plus_NDVI', red_band_num=3, nir_band_num=4)

# Loop through all TIF files within the Landsat ETM+ directory and apply the linear model.
array_list=[] # Create an empty list to store NumPy arrays of predicted values.
directory=r'D:\Fall_2018\GIS_Project\Presentation\Images\Plus_NDVI' # Set image directory.
for i in glob.glob(directory+"/*.TIF"): # Loop through TIF files in directory.
    y=rs_wq.apply_lm(raster=i,predictor_band_numbers=[2, 4],parameter_estimates=[-0.050018, 1.696873],intercept=1.328964,predictor_transformations=['None','np.log(x)'],response_backtransformation='np.exp(y)',mask_by_NoData=0,mask_by_cloud=9) # Apply linear model.
    array_list.append(y) # Append prediction array to the list of NumPy arrays.

# Plot it!
name_list=['2000','2005','2010'] # Create a list of image years.

# Map.
plt.figure() # Create new figure.
plt.suptitle('Chlorophyll a (ug/L)') # Add super title.
for j in range(len(array_list)): # Loop through the NumPy arrays stored in the list of arrays.
    plt.subplot(1, 3, j + 1) # Create a subplot.
    lower_pct=np.percentile(array_list[j].compressed(),5) # Apply a percentile stretch with a lower percentile of 5%...
    upper_pct=np.percentile(array_list[j].compressed(),95) # ...and an upper percentile of 95%.
    plt.imshow(array_list[j], norm=plt.Normalize(lower_pct, upper_pct), cmap='coolwarm') # Create a map.
    plt.axis('off') # Turn axis off.
    plt.colorbar() # Add a colorbar legend.
    plt.title(name_list[j]) # Add a name to the map.

# Boxplot.
array_list_flattened=[] # Create new empty list to store flattened array_list.
for j in array_list: # Loop through array_list.
    array_compressed=j.compressed() # Compress NumPy array to remove masked values.
    array_list_flattened.append(array_compressed.flatten()) # Flatten NumPy array.
plt.figure() # Create new figure.
plt.boxplot(array_list_flattened,showfliers=False,showmeans=True) # Create boxplot.
plt.xlabel('Year') # Label x-axis.
name_numbers=np.array(range(len(name_list)))+1 # Get an array of x-axis tick positions.
plt.xticks(name_numbers.tolist(),name_list) # Label x-axis ticks.
plt.title('Chlorophyll a (ug/L)') # Add a plot title.

# Loop through all TIF files within the Landsat ETM+ directory and apply a standard deviation stretch to display original images.
array_list=[] # Create an empty list to store stacked NumPy arrays of raster images.
for raster in glob.glob(directory+"/*.TIF"): # Loop through TIF files in directory.
    stretched_bands=rs_wq.apply_std_stretch(raster=raster,rgb=[4,3,2],n=2,NoData_value=0) # Apply a standard deviation stretch.
    array_list.append(stretched_bands) # Append stacked arrays to the list of NumPy arrays.

# Plot it!
name_list=['31 Jul. 2000','13 Jul. 2005','27 Jul. 2010'] # Create a list of image names.

# Map.
plt.figure() # Create new figure.
plt.suptitle('Color Infrared Landsat Images') # Add super title.
for j in range(len(array_list)): # Loop through the NumPy arrays stored in the list of arrays.
    plt.subplot(1, 3, j + 1) # Create a subplot.
    plt.imshow(array_list[j]) # Create a map.
    plt.axis('off') # Turn axis off.
    plt.title(name_list[j]) # Add a name to the map.
