#######################################################
### REMOTE SENSING FOR WATER QUALITY MODULE (rs_wq) ###
#######################################################

# Import modules
import os
from osgeo import gdal, ogr
import glob
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pylab
import scipy.stats as stats
import statsmodels.api as sm
from operator import itemgetter
import math

###################
### to_mosaic() ###
###################

# Function:

def to_mosaic(inputdirectory,outputdirectory,sixteen_bit=False,shapefile_to_clip_to='None'):
    
    if (str(sixteen_bit) != 'True' and str(sixteen_bit) != 'False'):
        raise RuntimeError('sixteen_bit argument takes True or False')
    
    for i in os.listdir(inputdirectory): # Loop through folders in input directory.
        first_TIF=glob.glob(os.path.join(inputdirectory,i)+"/*.TIF")[0] # Get first TIF file in folder.
        
        if shapefile_to_clip_to != 'None': # If a clipping shapefile is specified.
            in_ds=clip(raster=first_TIF,shapefile=shapefile_to_clip_to) # Open first TIF in folder as a clipped datasource.
        else:
            in_ds=gdal.Open(first_TIF) # Open first TIF in folder.
        
        # Check number of bands to include in new GeoTIFF file.
        num_bands=0 # Set number of bands in output mosaic raster to 0.
        for k in glob.glob(os.path.join(inputdirectory,i)+"/*.TIF"): # Loop through TIF files in folders.
            
            if shapefile_to_clip_to != 'None': # If a clipping shapefile is specified.
                other_ds=clip(raster=k,shapefile=shapefile_to_clip_to) # Open TIF file as a clipped datasource.
                if (other_ds.RasterYSize == in_ds.RasterYSize and other_ds.RasterXSize == in_ds.RasterXSize):
                    num_bands+=1 # If the raster size is the same as the first raster's, then add a 1 to the number of bands.
                del other_ds # Delete other_ds

            else:
                other_ds=gdal.Open(k) # Open TIF file.
                if (other_ds.RasterYSize == in_ds.RasterYSize and other_ds.RasterXSize == in_ds.RasterXSize):
                    num_bands+=1 # If the raster size is the same as the first raster's, then add a 1 to the number of bands.
                del other_ds # Delete other_ds
                
        # Create new GeoTIFF file.
        fn=os.path.join(outputdirectory,i+".tif")
        
        # Create new GeoTIFF file.
        if sixteen_bit == False: # If data type unsigned 16-bit if not asked for.
            out_ds=gdal.GetDriverByName('GTiff').Create(fn, in_ds.RasterXSize, in_ds.RasterYSize, num_bands) # Create new GeoTIFF file using folder name. Set raster size to size of first TIF. Number of bands equal to num_bands.
        if sixteen_bit == True: # Use data type unsigned 16-bit if asked for.
            out_ds=gdal.GetDriverByName('GTiff').Create(fn, in_ds.RasterXSize, in_ds.RasterYSize, num_bands, gdal.GDT_UInt16) # Create new GeoTIFF file using folder name. Set raster size to size of first TIF. Number of bands equal to num_bands.
        
        out_ds.SetProjection(in_ds.GetProjection()) # Copy projection from first TIF file.
        out_ds.SetGeoTransform(in_ds.GetGeoTransform()) # Copy geotransform from first TIF file.
        del in_ds # Delete in_ds
        print "File name: " + i + ".tif" # Print out name of new GeoTIFF file.
        print "---"
        
        # Add bands to GeoTIFF file.
        j=1 # Start band number at 1.
        for k in glob.glob(os.path.join(inputdirectory,i)+"/*.TIF"): # Loop through TIF files in folders.
            if shapefile_to_clip_to != 'None':
                in_ds=clip(raster=k,shapefile=shapefile_to_clip_to) # Open TIF file as a clipped datasource.
            else:
                in_ds=gdal.Open(k) # Open TIF file.
            if (in_ds.RasterYSize == out_ds.RasterYSize and in_ds.RasterXSize == out_ds.RasterXSize): # Check to make sure each band is the same size as the first.
                in_data=in_ds.GetRasterBand(1).ReadAsArray() # Read band into memory.
                out_ds.GetRasterBand(j).WriteArray(in_data) # Write band to new GeoTIFF file.
                out_ds.GetRasterBand(j).FlushCache() # Flush cache.
                out_ds.GetRasterBand(j).ComputeStatistics(False) # Compute band statistics.
                name=os.path.basename(k) # Get filename.
                name="B" + name.split("B")[1][:-4] # Get band name from filename.
                print "Band " + str(j) + ": " + name # Print band number and associated band name.
                j+=1 # Add 1 to the band numer.
        print "###"
        del in_ds # Delete in_ds
        del out_ds # Delete out_ds
    print "Done!"

# Description:
    # Given an input directory which contains folder(s) which each contain all the bands of a raster image, creates a single new mosaic image for each folder.
    # The file name of the new mosaic image is that of the folder which contains the image's bands.
    # New mosaic images are saved to a specified output directory.
    # Which image band is stored in each band number of the new mosaic image is printed along with the file name of the mosaic image.
    # The panchromatic band (band 8) of Landsat 7 and Landsat 8 is not included in the mosaic image since it is a different resolution than the other bands.
    # The data type of the mosaic image can be either 16-bit unsigned integer or 8-bit unsigned integer depending on if 16-bit is asked for.
    # The mosaic image can be clipped to a specified raster if clipping is asked for.
    
# Required formatting:
    # All bands corresponding to a particular image are stored in the same folder and no bands from other images are stored in this folder.
        # Either a single folder or multiple folders are stored in the same location.
    # If clipping is asked for, the shapefile to clip to and the rasters must be of the same spatial reference system.

# Arguments:
    # inputdirectory: A directory specifying the location where folder(s) containing image bands are stored.
    # outputdirctory: The directory to which new mosaic images will be saved.
    # sixteen_bit (default = False, alternative option = True): If set to True, the mosaic image will be in 16-bit unsigned integer (this preserves the quality assessment band). If set to False, the mosaic image will be in 8-bit unsigned integer.

###############
### cloud() ###
###############

# Function:

def cloud(array):
    def cloud_unvec(num):
        binary=bin(num)[2:] # Get the number as a binary.
        if (len(binary) >= 5): # If the binary has five or more bits.
            cloud=binary[len(binary)-5] # Get bit #4 (5th position from the right).
        else:
            cloud=0 # If bit #4 is not included in the binary, then there are no clouds over this pixel.
        return int(cloud) # Return a 0 or 1 indicating of there is cloud covering the pixel.
    array=array.astype(np.uint16) # Make sure data type is unsigned 16-bit.
    vcloud=np.vectorize(cloud_unvec) # Vectorize cloud function.
    return vcloud(array) # Return an array of 0 and 1s specifying whether there is cloud covering each pixel.

# Description:
    # Given a numpy array of pixel values from a Landsat quality assessment band (BQA), return a numpy array with 0 (no cloud cover) or 1 (cloud cover) for each pixel value.

# Required formatting:
    # Input is a numpy array of a Landsat quality assessment band (BQA).

# Arguments:
    # array: A numpy array of a Landsat quality assessment band (BQA).

##############
### clip() ###
##############

# Function:

def clip(raster,shapefile):
    # Open the shapefile.
    shp_ds = ogr.Open(shapefile)
    if not shp_ds:
        raise IOError('Could not open shapefile')
        
    # Get the shapefile's extent.
    min_x, max_x, min_y, max_y = shp_ds.GetLayer(0).GetExtent()
    
    # Open the raster.
    raster_ds = gdal.Open(raster)
    if not raster_ds:
        raise IOError('Could not open raster')
        
    # Get the geotransform and the inverse geotransform from the raster.
    geotransform = raster_ds.GetGeoTransform()
    inverse_geotransform = gdal.InvGeoTransform(geotransform)
    if inverse_geotransform is None:
        raise RuntimeError('Could not calculate inverse geotransform')
        
    # Get the raster pixel offsets (as integers) for the upper left and lower right corners
    # of the shapefile's extent.
    ulx_off, uly_off = map(int, gdal.ApplyGeoTransform(inverse_geotransform, min_x, max_y))
    lrx_off, lry_off = map(int, gdal.ApplyGeoTransform(inverse_geotransform, max_x, min_y))
    
    # Increase the lower right offsets.
    lrx_off += 1
    lry_off += 1
    
    # Get the real-world coordinates corresponding to the pixel offsets.
    ulx, uly = gdal.ApplyGeoTransform(geotransform, ulx_off, uly_off)
    lrx, lry = gdal.ApplyGeoTransform(geotransform, lrx_off, lry_off)
    
    # Calculate the numbers of rows and columns.
    rows = int((uly - lry) / -geotransform[5])
    cols = int((lrx - ulx) / geotransform[1])
    
    # Create the new geotransform.
    new_geotransform = [ulx, geotransform[1], geotransform[2], uly, geotransform[4], geotransform[5]]
    
    # Create the output raster using the numbers of rows and columns calculated above. Get the number
    # of bands, data type, and projection info from the raster.
    driver = gdal.GetDriverByName('MEM')
    clip_ds = driver.Create('clipped', cols, rows, raster_ds.RasterCount, raster_ds.GetRasterBand(1).DataType)
    clip_ds.SetProjection(raster_ds.GetProjection())
    
    # Add the new geotransform.
    clip_ds.SetGeoTransform(new_geotransform)
    
    # Use the memory driver to create a temporary raster with the same
    # dimensions and geotransform as the output raster.
    temp_ds = gdal.GetDriverByName('MEM').Create('temp', cols, rows)
    temp_ds.SetGeoTransform(new_geotransform)
    
    # Rasterize the first layer from shp_ds into band 1 of temp_ds. All pixels 
    # inside the polygons in shp_ds get values of 1 (burn_values).
    shp_lyr = shp_ds.GetLayer(0)
    gdal.RasterizeLayer(temp_ds, [1], shp_lyr, burn_values=[1])
    
    # Now we can close the shapefile because we're done with it.
    del shp_ds
    
    # Read in output from RasterizeLayer as a numpy array.
    mask_data = temp_ds.GetRasterBand(1).ReadAsArray()
        
    # Loop enough times for each band in the aster image.
    for i in range(raster_ds.RasterCount):
        
        # Get the ith band from the raster image.
        raster_band = raster_ds.GetRasterBand(i + 1)
        
        # Get the ith band from the new raster.
        clip_band = clip_ds.GetRasterBand(i + 1)
        
        # Read the data from the raster band. Start at the pixel offsets calculated
        # earlier and read the number of rows and columns calculated earlier. Remember
        # that these correspond to the extent of the shapefile.
        data = raster_band.ReadAsArray(ulx_off, uly_off, cols, rows)
        
        # Mask out the pixels where mask_data is 0 and then fill in the masked pixels with 0.
        data = np.ma.masked_where(mask_data == 0, data)
        data = data.filled(0)
        
        # Write the data to the new band.
        clip_band.WriteArray(data)
        
        # Calculate statistics for the band.
        clip_band.FlushCache()
        clip_band.ComputeStatistics(False)
        
    return clip_ds # Return the datasource.
    
    # Close the datasets after the loop is done.
    del raster_ds, clip_ds

# Description:
    # Clips a raster to a shapefile and returns a datasource of a clipped raster stored in memory.

# Required formatting:
    # The raster and shapefile have the same spatial reference system.

# Arguments:
    # raster: Directory to the raster file to be clipped.
    # shapefile: Directory to the shapefile which the raster will be clipped to.

################################
### extract_values_at_points ###
################################

# Function:

def extract_values_at_points(raster,points_shapefile,point_identifier,clip_shapefile='None',mask_by_cloud=False,mask_by_NoData=False,window_size=1):
    
    # Check to make sure that the mask by cloud argument is either True or False.
    if (str(mask_by_cloud) != 'True' and str(mask_by_cloud) != 'False'):
        raise RuntimeError('mask_by_cloud argument takes True or False')
    
    # Check window size.
    if window_size % 2 == 0:
        raise RuntimeError('window_size must be odd')
    
    # Open the sites shapefile and put it in the sites_ds variable.
    sites_ds = ogr.Open(points_shapefile)

    # Check to make sure that it was able to open the file.
    if sites_ds is None:
        raise RuntimeError('Could not open points shapefile')
    
    # Get the layer from the sites shapefile (sites_ds).
    lyr = sites_ds.GetLayer(0)
    
    # Store the value of each point identifier.
    id = [row.GetField(point_identifier) for row in lyr]
    lyr.ResetReading() # Reset layer reading.
    
    # Create a big list.
    all_values=[]
    
    # Store the point identifiers in the big list.
    all_values.append(id)
    
    # Clip by clip shapefile if specified.
    if clip_shapefile == 'None':
        ds=gdal.Open(raster)
    else:
        ds=clip(raster=raster,shapefile=clip_shapefile)
        
    # Create geotransform for raster file.
    geotransform = ds.GetGeoTransform()
    
    # Get the inverse geotransform (converts real-world coordinates to pixel offsets).
    inverse_geotransform = gdal.InvGeoTransform(geotransform)
    if inverse_geotransform is None:
        raise RuntimeError('Could not calculate inverse geotransform')
    
    # Get first band if mask_by_NoData is not False.
    if str(mask_by_NoData) != 'False':
        first_band=ds.GetRasterBand(1).ReadAsArray()
    
    # Get cloud band if mask_by_cloud is True.
    if mask_by_cloud == True:
        bqa=ds.GetRasterBand(ds.RasterCount).ReadAsArray()
        cloud_band=cloud(bqa)
        
    # Loop through the bands in the raster file.
    for i in range(ds.RasterCount):
        
        # Read in a band.
        band=ds.GetRasterBand(i + 1).ReadAsArray()
        
        # Mask by no data value if mask_by_NoData is not False.
        if str(mask_by_NoData) != 'False':
            band=np.ma.masked_where(first_band==mask_by_NoData,band)
        
        # Mask by cloud if mask_by_cloud is True.
        if mask_by_cloud == True:
            band=np.ma.masked_where(cloud_band==1,band)
                
        # Create empty list to store raster band average values.
        values = []
        
        # Loop through the rows in the points shapefile layer.
        for row in lyr:
            point = row.geometry() # Get point geometry
            x = point.GetX() # Store the value of the x coordinate
            y = point.GetY() # Store the value of the y coordinate
            
            # Use the inverse geotransform to get pixel offsets for the x,y coordinates
            x, y = gdal.ApplyGeoTransform(inverse_geotransform, x, y)
            x = int(x) # Turn x pixel offset into an integer
            y = int(y) # Turn y pixel offset into an integer
            
            # Read band data from pixel offsets
            raster_values=band[y - int(window_size/2):y+int(window_size/2)+1,x - int(window_size/2):x + int(window_size/2)+1] # Extract values from band.
            value = np.mean(raster_values) # Take average of the extracted values.
            
            values.append(value) # Store value in values list.
            
        # Store values list in a list of lists.
        all_values.append(values)
        
        # Reset layer reading
        lyr.ResetReading()
    
    # Turn big list into a Pandas data frame.
    df_1=pd.DataFrame(all_values)
    
    # Transpose data frame.
    df_2=df_1.transpose()
    
    # Create a new column names list.
    new_names=[point_identifier]
    
    # Add band numbers to the column names list.
    for h in range(ds.RasterCount):
        band_number=str(h + 1)
        new_names.append('Band_' + band_number)
    
    # Rename data frame columns.
    df_2.columns=new_names
    
    # Delete datasources
    del sites_ds, ds
    
    # Return results data frame.
    return df_2

# Description:
    # Extracts raster pixel values at point from a shapefile. The pixel values are returned as a Pandas data frame. The function extracts pixel values from all bands in a raster and stores them in the data frame. A point identification field is taken from the point shapefile's attribute table to include in the returned data frame. The average of a window of pixel values will be sampled using the specified window_size if window_size is provided. Otherwise, pixels will be sampled at the points. Options also exist to mask out no data pixels, to mask out pixels covered by clouds (this uses the Landsat BQA [quality assessment] band, which is assumed to be the last band in the raster file), and to crop the raster image to a polygon shapefile prior to masking or pixel sampling (this can increase processing speed).

# Required formatting:
    # The raster and shapefiles share the same spatial reference system.
    # To use the mask_by_cloud option, a Landsat BQA (quality assessment) band must be included as the last band in the raster image.

# Arguments:
    # raster: Directory to the raster file to be clipped.
    # points_shapefile: Directory to the shapefile containing points at which raster pixel values will be extracted.
    # point_identifier: Field in the attribute table of the points_shapefile which will be used to identify the points at which values were extracted. This field will appear in the returned data frame.
    # clip_shapefile (default = 'None'): Directory to the shapefile which the raster will be clipped to. If left as default, the raster image will not be clipped to this shapefile prior to masking or pixel sampling, which may increase processing time.
    # mask_by_cloud (default = False, alternative option = True): If True, pixels identified using the Landsat BQA (quality assessment) band as being covered by cloud will be masked out of the raster image. If False, clouded pixels will not be masked out.
    # mask_by_NoData (default = False, alternative option = specify a no data value): If a no data value is specified, no data pixels with the specified value will be masked out of the raster image. If False, no data pixels will not be masked out.
    # window_size (default = 1): An integer specifying the size of the pixel sampling window. This value represents the length of one side of a square sampling window. With the default of 1, only a single pixel will be sampled. If more than 1 pixel is sampled, the average of the pixel values will be returned in the data frame.
    
##################
### normplot() ###
##################

# Function:

def normplot(df):
    colnames=df.columns # Get column names.
    for i in colnames: # Loop through column names.
        values=np.array(df[str(i)]) # Create a NumPy array of the specified column.
        values=values[~pd.isnull(values)] # Remove nan values from array.
        values=values.astype(float) # Turn values to float.
        plt.figure() # Create a figure.
        plt.suptitle(i) # Add a figure supertitle.
        plt.subplot(1,3,1) # Create subplot.
        plt.hist(values,bins=20) # Create histogram with 20 bins.
        plt.title('Histogram') # Add subplot title.
        plt.subplot(1,3,2) # Create subplot.
        stats.probplot(values,dist='norm',plot=pylab) # Create QQ plot.
        plt.title('QQ Plot') # Add subplot title.
        plt.subplot(1,3,3) # Create subplot.
        plt.boxplot(values) # Create boxplot.
        plt.title('Boxplot') # Add subplot title.
    plt.figure() # Create a new figure.

# Description:
    # Creates a histogram, QQ plot, and boxplot for each column of a Pandas data frame. This is useful for checking the distributions of variables.

# Required formatting:
    # Each variable to be plotted should stored in a column of a Pandas data frame.

# Arguments:
    # df: A Pandas data frame which contains a variable in each column.

##############
### corr() ###
##############

# Function:

def corr(df,exclude):
    corr=np.corrcoef(df.drop(exclude,1),rowvar=0) # Create a correlation matrix.
    corr=pd.DataFrame(corr) # Turn matrix in a Pandas data frame.
    corr.columns=df.drop(exclude,1).columns # Rename data frame columns.
    corr.index=df.drop(exclude,1).columns # Rename data frame rows.
    return corr # Return the data frame.

# Description:
    # Given a Pandas data frame, returns a labelled correlation matrix of the variables. Excludes specified variables.

# Required formatting:
    # A Pandas data frame must be provided.
    # A list of column names to exclude from the correlation matrix must be provided.

# Arguments:
    # df: A Pandas data frame with variables stored in columns.
    # exclude: A list of column names to exclude from the correlation matrix.

#####################
### scatterplot() ###
#####################

# Function:

def scatterplot(df,y):
    for i in df.columns.drop([y],1): # Loop through predictor variable columns.
        plt.figure() # Create new plot.
        plt.scatter(df[[i]],df[[y]]) # Create a scatter plot of the response versus the predictor.
        plt.title(y + ' by ' + i) # Add a title.
        plt.xlabel(i) # Add an x-axis label.
        plt.ylabel(y) # Add a y-axis label.
        fit=np.polyfit(df[i],df[y],1) # Fit a trendline.
        func=np.poly1d(fit) # Prepare the trendline.
        plt.plot(df[[i]],func(df[[i]]),'r--') # Plot the trendline.
    plt.figure() # Create new plot.

# Description:
    # Given a Pandas data frame, create scatter plots between a single response variable and multiple predictor variables.

# Required formatting:
    # A Pandas data frame with variables stored in columns. There may only be one response variable included in the data frame.

# Arguments:
    # df: A Pandas data frame with variables stored in columns.
    # y: The name of the column which contains the response variable.

#################
### lm_full() ###
#################

# Function:

def lm_full(df,y):
    df=df.astype(float) # Turn data frame into float type.
    x=df.drop(y,1) # Create an x data frame.
    x=sm.add_constant(x) # Add a constant to the x data frame.
    x.rename(columns={'const':'Intercept'},inplace=True) # Rename the costant Intercept.
    y_val=df[y] # Create a y data frame.
    lm=sm.OLS(y_val,x).fit() # Fit a linear model.
    
    print '###'
    print '- Full Linear Regression Results -'
    print '###'
    print 'R-Squared:'
    print '---'
    print lm.rsquared
    print '###'
    print 'Adjusted R-Squared:'
    print '---'
    print lm.rsquared_adj
    print '###'
    print 'AIC:'
    print '---'
    print lm.aic
    print '###'
    print 'Parameter Estimates:'
    print '---'
    print lm.params
    print '###'
    print 'P-Values:'
    print '---'
    print lm.pvalues
    print '###'
    
    # Create plot of actual against predicted values.
    pred=lm.predict(x) # Predict values.        
    plt.figure() # Create new plot.
    plt.scatter(y_val, pred) # Create a scatter plot of the predicted versus actual response values.
    plt.title('Predicted Values by ' + y) # Add a title.
    plt.xlabel(y) # Add an x-axis label.
    plt.ylabel('Predicted Values') # Add a y-axis label.
    fit=np.polyfit(y_val,pred,1) # Fit a trendline.
    func=np.poly1d(fit) # Prepare the trendline.
    plt.plot(y_val,func(y_val),'r--') # Plot the trendline.
    plt.figure() # Create new plot.

# Description:
    # Given a Pandas data frame, fit a linear regression model of the specified response variable on all other variables in the data frame.

# Required formatting:
    # A Pandas data frame with variables stored in columns. There may only be one response variable included in the data frame.

# Arguments:
    # df: A Pandas data frame with variables stored in columns.
    # y: The name of the column which contains the response variable.

#####################
### lm_backward() ###
#####################

# Function:

def lm_backward(df,y,p_value_to_include=0.15):
    df=df.astype(float) # Turn data frame into float type.
    x=df.drop(y,1) # Create an x data frame.
    x=sm.add_constant(x) # Add a constant to the x data frame.
    x.rename(columns={'const':'Intercept'},inplace=True) # Rename the constant Intercept.
    y_val=df[y] # Create a y data frame.
    lm=sm.OLS(y_val,x).fit() # Fit a linear model.
    
    end=0 # Set end to 0.
    while end==0: # While end is equal to 0.
        p_num=0 # Set p_num to 0.
        p_values_too_large=[] # Create an empty list to store p-values which are too large.
        for h in lm.pvalues: # Loop through linear model p-values.
            if p_num != 0: # Skip the first p-value (which is for the intercept).
                if h > p_value_to_include: # Check to see if the p-value is too large.
                    p_values_too_large.append([p_num,h]) # Add p-values which are too large to a list.
            p_num+=1 # Increase p_num by 1.
        if len(p_values_too_large) != 0: # If there are p-values which are too large.
            p_values_too_large=sorted(p_values_too_large,key=itemgetter(1),reverse=True) # Sort list of p-values too so that the largest p-value is firt in the list.
            index_to_exclude=p_values_too_large[0][0] # Get the column index of the variable with the highest p-value.
            var_name_to_exclude=x.iloc[:,index_to_exclude].name # Get the variable name of this column index.
            x=x.drop(var_name_to_exclude,1) # Exclude this variable from the predictor variable data frame.
            lm=sm.OLS(y_val,x).fit() # Fit a linear model with the least significant variables removed.
        
        else:
            end=1 # If there are no more variables which have p-values which are too large, then end the loop.
            
            print '###'
            print '- Backward Selection Linear Regression Results -'
            print '###'
            print 'R-Squared:'
            print '---'
            print lm.rsquared
            print '###'
            print 'Adjusted R-Squared:'
            print '---'
            print lm.rsquared_adj
            print '###'
            print 'AIC:'
            print '---'
            print lm.aic
            print '###'
            print 'Parameter Estimates:'
            print '---'
            print lm.params
            print '###'
            print 'P-Values:'
            print '---'
            print lm.pvalues
            print '###'
            
            # Create plot of actual against predicted values.
            pred=lm.predict(x) # Predict values.        
            plt.figure() # Create new plot.
            plt.scatter(y_val, pred) # Create a scatter plot of the predicted versus actual response values.
            plt.title('Predicted Values by ' + y) # Add a title.
            plt.xlabel(y) # Add an x-axis label.
            plt.ylabel('Predicted Values') # Add a y-axis label.
            fit=np.polyfit(y_val,pred,1) # Fit a trendline.
            func=np.poly1d(fit) # Prepare the trendline.
            plt.plot(y_val,func(y_val),'r--') # Plot the trendline.
            plt.figure() # Create new plot.

# Description:
    # Given a Pandas data frame, fit a linear regression model of the specified response variable on all other variables in the data frame using backward elimination variable selection. The least significant predictor is repeatedly removed until all predictors have p-values less than or equal to the specified p-value threshold.

# Required formatting:
    # A Pandas data frame with variables stored in columns. There may only be one response variable included in the data frame.

# Arguments:
    # df: A Pandas data frame with variables stored in columns.
    # y: The name of the column which contains the response variable.
    # p_value_to_include (default = 0.15): All predictors in the final model will have p-values less than or equal to this p-value.

####################
### lm_forward() ###
####################

# Function:

def lm_forward(df,y,p_value_to_include=0.15):
    df=df.astype(float) # Turn data frame into float type.
    x=df.drop(y,1) # Create an x data frame.
    var_names=x.columns # Get predictor names.
    x=sm.add_constant(x) # Add a constant to the x data frame.
    x.rename(columns={'const':'Intercept'},inplace=True) # Rename the constant Intercept.
    x_new=x.drop(var_names,1) # Keep just the Intercept term.
    y_val=df[y] # Create a y data frame.
    
    p_val_list=[] # Create an empty list to store p-values.
    for i in var_names: # Loop through the predictor variables.
        lm_simple=sm.OLS(y_val,x[['Intercept',i]]).fit() # Fit a simple linear model for each predictor.
        p_val_list.append([i, lm_simple.pvalues[1]]) # Get the p-value for each simple linear model.
    p_val_list=sorted(p_val_list,key=itemgetter(1)) # Sort the p-values from the simple linear models.
    for k in range(len(p_val_list)): # Loop through the p-values in the list.
        var_name_to_include=p_val_list[k][0] # Get the predictor's name.
        x_new[var_name_to_include]=x[var_name_to_include] # Add the predictor to a new x data frame.
        lm=sm.OLS(y_val,x_new).fit() # Fit a linear model on the new x data frame.
        if lm.pvalues[len(lm.pvalues)-1] > p_value_to_include: # If the p-value of the most recently added predictor is greater than the specified p-value cutoff.
            x_new=x_new.drop(var_name_to_include,1) # Then drop this predictor from the new x data frame.
    lm=sm.OLS(y_val,x_new).fit() # Fit a final linear model.
    print '###'
    print '- Forward Selection Linear Regression Results -'
    print '###'
    print 'R-Squared:'
    print '---'
    print lm.rsquared
    print '###'
    print 'Adjusted R-Squared:'
    print '---'
    print lm.rsquared_adj
    print '###'
    print 'AIC:'
    print '---'
    print lm.aic
    print '###'
    print 'Parameter Estimates:'
    print '---'
    print lm.params
    print '###'
    print 'P-Values:'
    print '---'
    print lm.pvalues
    print '###'
    
    # Create plot of actual against predicted values.
    pred=lm.predict(x_new) # Predict values.        
    plt.figure() # Create new plot.
    plt.scatter(y_val, pred) # Create a scatter plot of the predicted versus actual response values.
    plt.title('Predicted Values by ' + y) # Add a title.
    plt.xlabel(y) # Add an x-axis label.
    plt.ylabel('Predicted Values') # Add a y-axis label.
    fit=np.polyfit(y_val,pred,1) # Fit a trendline.
    func=np.poly1d(fit) # Prepare the trendline.
    plt.plot(y_val,func(y_val),'r--') # Plot the trendline.
    plt.figure() # Create new plot.

# Description:
    # Given a Pandas data frame, fit a linear regression model of the specified response variable on all other variables in the data frame using forward variable selection. A simple linear regression model is fit on each predictor, and the predictors are added to the multiple linear regression model starting with the most significant predictor from the simple linear regression models. When each predictor is added to the multiple linear regression model, its p-value in this model is checked, and if the p-value exceeds the specified p-value threshold, then it is removed from the model.

# Required formatting:
    # A Pandas data frame with variables stored in columns. There may only be one response variable included in the data frame.

# Arguments:
    # df: A Pandas data frame with variables stored in columns.
    # y: The name of the column which contains the response variable.
    # p_value_to_include (default = 0.15): As predictors are added to the model, their p-values are checked, and if their p-values exceed this specified threshold p-value, then they are removed from the model.

#################
### lm_step() ###
#################

# Function:

def lm_step(df,y,p_value_to_include=0.15):
    df=df.astype(float) # Turn data frame into float type.
    x=df.drop(y,1) # Create an x data frame.
    var_names=x.columns # Get predictor names.
    x=sm.add_constant(x) # Add a constant to the x data frame.
    x.rename(columns={'const':'Intercept'},inplace=True) # Rename the constant Intercept.
    x_new=x.drop(var_names,1) # Keep just the Intercept term.
    y_val=df[y] # Create a y data frame.
    
    p_val_list=[] # Create an empty list to store p-values.
    for i in var_names: # Loop through the predictor variables.
        lm_simple=sm.OLS(y_val,x[['Intercept',i]]).fit() # Fit a simple linear model for each predictor.
        p_val_list.append([i, lm_simple.pvalues[1]]) # Get the p-value for each simple linear model.
    p_val_list=sorted(p_val_list,key=itemgetter(1)) # Sort the p-values from the simple linear models.
    for k in range(len(p_val_list)): # Loop through the p-values in the list.
        var_name_to_include=p_val_list[k][0] # Get the predictor's name.
        x_new[var_name_to_include]=x[var_name_to_include] # Add the predictor to a new x data frame.
        lm=sm.OLS(y_val,x_new).fit() # Fit a linear model on the new x data frame.
        if lm.pvalues[len(lm.pvalues)-1] > p_value_to_include: # If the p-value of the most recently added predictor is greater than the specified p-value cutoff.
            x_new=x_new.drop(var_name_to_include,1) # Then drop this predictor from the new x data frame.
        lm=sm.OLS(y_val,x_new).fit() # Fit a linear model after variables have been decided for this step in the loop.
        lm_pval_num=0 # Set the p-value index number to 0.
        lm_pval_num_list=[] # Create an empty list to store p-values and their indices.
        for l in lm.pvalues: # Loop through the p-values of the most recently fit linear model.
            if lm_pval_num != 0: # Skip the first p-value index (which is the intercept).
                lm_pval_num_list.append([lm_pval_num,l]) # Store the p-value index and p-value itself in the storage list.
            lm_pval_num+=1 # Add 1 to the p-value index.
        lm_pval_too_large=[u for u in lm_pval_num_list if u[1] > p_value_to_include] # Subset the list to just p-values which are too large.
        indices_to_exclude=[ind[0] for ind in lm_pval_too_large] # Get the indices of the p-values which are too large.
        if len(indices_to_exclude) > 1: # If there are multiple p-value indices to exclude.
            var_names_to_exclude=x.iloc[:,indices_to_exclude].columns # Get the names of variables to exclude from their p-value indices.
        elif len(indices_to_exclude) == 1: # If there is a single p-value index to exclude.
            var_names_to_exclude=x.iloc[:,indices_to_exclude].name # Get the name of the variable to exclude from the p-value index.
        else: # If there are no p-value indices to exclude.
            var_names_to_exclude=[] # Leave the variable names to exclude list empty.
        x_new=x_new.drop(var_names_to_exclude,1) # Drop the variables from the new x data frame whose p-values are too large.
        
    lm=sm.OLS(y_val,x_new).fit() # Fit a final linear model.
    print '###'
    print '- Forward Selection Linear Regression Results -'
    print '###'
    print 'R-Squared:'
    print '---'
    print lm.rsquared
    print '###'
    print 'Adjusted R-Squared:'
    print '---'
    print lm.rsquared_adj
    print '###'
    print 'AIC:'
    print '---'
    print lm.aic
    print '###'
    print 'Parameter Estimates:'
    print '---'
    print lm.params
    print '###'
    print 'P-Values:'
    print '---'
    print lm.pvalues
    print '###'
    
    # Create plot of actual against predicted values.
    pred=lm.predict(x_new) # Predict values.        
    plt.figure() # Create new plot.
    plt.scatter(y_val, pred) # Create a scatter plot of the predicted versus actual response values.
    plt.title('Predicted Values by ' + y) # Add a title.
    plt.xlabel(y) # Add an x-axis label.
    plt.ylabel('Predicted Values') # Add a y-axis label.
    fit=np.polyfit(y_val,pred,1) # Fit a trendline.
    func=np.poly1d(fit) # Prepare the trendline.
    plt.plot(y_val,func(y_val),'r--') # Plot the trendline.
    plt.figure() # Create new plot.

# Description:
    # Given a Pandas data frame, fit a linear regression model of the specified response variable on all other variables in the data frame using stepwise variable selection. A simple linear regression model is fit on each predictor, and the predictors are added to the multiple linear regression model starting with the most significant predictor from the simple linear regression models. When each predictor is added to the multiple linear regression model, the p-values of all predictors in this model are checked, and if the p-value of any predictor exceeds the specified p-value threshold, then the predictor is removed from the model.

# Required formatting:
    # A Pandas data frame with variables stored in columns. There may only be one response variable included in the data frame.

# Arguments:
    # df: A Pandas data frame with variables stored in columns.
    # y: The name of the column which contains the response variable.
    # p_value_to_include (default = 0.15): As predictors are added to the model, the p-values of all predictors in the model are checked, and if any predictor's p-value exceeds this specified threshold p-value, then the predictor is removed from the model.

##################
### add_ndvi() ###
##################

# Function:

def add_ndvi(inputdirectory,outputdirectory,red_band_num,nir_band_num):
    print "###"
    for k in glob.glob(inputdirectory+"/*.TIF"): # Loop through TIF files in input directory.
        in_ds=gdal.Open(k) # Open TIF file.
        red=in_ds.GetRasterBand(red_band_num).ReadAsArray() # Read red band into memory.
        nir=in_ds.GetRasterBand(nir_band_num).ReadAsArray().astype(np.float32) # Read NIR band into memory and convert to floating point.
        red=np.ma.masked_where(nir + red == 0, red) # Mask out where the NDVI denominator will be 0.
        ndvi=(nir - red)/(nir + red) # Calculate NDVI.
        ndvi=ndvi.filled(0) # Fill in masked pixels with 0 as the no data value.
        ndvi_band_num=in_ds.RasterCount + 1 # Set NDVI band number to an additional band.
        name=os.path.basename(k) # Get filename.
        fn=os.path.join(outputdirectory,name) # Create output file name and directory.
        out_ds=gdal.GetDriverByName('GTiff').Create(fn, in_ds.RasterXSize, in_ds.RasterYSize, ndvi_band_num, gdal.GDT_Float32) # Create new GeoTIFF file in output directory with an added band.
        out_ds.SetProjection(in_ds.GetProjection()) # Copy projection from first TIF file.
        out_ds.SetGeoTransform(in_ds.GetGeoTransform()) # Copy geotransform from first TIF file.
        for i in range(in_ds.RasterCount): # Loop through bands in input file.
            in_data=in_ds.GetRasterBand(i + 1).ReadAsArray() # Read in band in input file.
            out_ds.GetRasterBand(i + 1).WriteArray(in_data) # Write the band to the new TIF file.
            out_ds.GetRasterBand(i + 1).FlushCache() # Flush cache.
            out_ds.GetRasterBand(i + 1).ComputeStatistics(False) # Compute band statistics.
        out_ds.GetRasterBand(ndvi_band_num).WriteArray(ndvi) # Write NDVI band to the new TIF file.
        out_ds.GetRasterBand(ndvi_band_num).FlushCache() # Flush cache.
        out_ds.GetRasterBand(ndvi_band_num).ComputeStatistics(False) # Compute band statistics.
        print "NDVI added as band " + str(ndvi_band_num) + " to a copy of " + name + " in output directory." # Print NDVI band number with file name.
        print "---"
        del in_ds, out_ds # Delete data sources.
    print "Done!"
    print "###"

# Description:
    # Given an input directory which contains mosaic image(s), copies the mosaic image(s) to the output directory and appends an NDVI band to the new raster file.
    # The band number of the new NDVI band is printed along with the new raster's filename.
    # The data type of the new raster file will be float 32.

# Required formatting:
    # The input directory contains mosaic images(s).

# Arguments:
    # inputdirectory: A directory specifying the location where mosaic images are stored.
    # outputdirctory: The directory to which new raster files with an appended NDVI band will be saved.
    # red_band_num: The red band number of the mosaic images in the input directory.
    # nir_band_num: The NIR band number of the mosaic images in the input directory.

##################
### apply_lm() ###
##################

# Function:

def apply_lm(raster,predictor_band_numbers,parameter_estimates,intercept,predictor_transformations,response_backtransformation='None',mask_by_NoData=False,mask_by_cloud=False):
    ds=gdal.Open(raster) # Open TIF file.
    if not ds: # Check to make sure file was opened.
        raise IOError('Could not open raster')
            
    # Get cloud band if mask_by_cloud is not False.
    if str(mask_by_cloud) != 'False':
        bqa=ds.GetRasterBand(mask_by_cloud).ReadAsArray()
        cloud_band=cloud(bqa)
    
    # Get raster shape.
    raster_shape=ds.GetRasterBand(1).ReadAsArray().shape
    
    # Create empty response NumPy array with the raster's shape.
    y=np.zeros(shape=raster_shape)
    
    # Mask entire response array.
    y=np.ma.masked_where(y==0,y)
    
    # Fill entire response array with the intercept value.
    y=y.filled(intercept)
    
    # Loop through the predictor bands.
    for i in range(len(predictor_band_numbers)):
        x=ds.GetRasterBand(predictor_band_numbers[i]).ReadAsArray() # Read band into memory.
        
        # Mask by no data value if mask_by_NoData is not False.
        if str(mask_by_NoData) != 'False':
            x=np.ma.masked_where(x==mask_by_NoData,x) # Mask out no data values.
        
        # Mask by cloud if mask_by_cloud is not False.
        if str(mask_by_cloud) != 'False':
            x=np.ma.masked_where(cloud_band==1,x) # Mask out clouded pixels.
    
        # Transform the band if asked for.
        if predictor_transformations[i] != 'None':
            x=eval(predictor_transformations[i])
            
        # Multiply predictor by parameter estimate.
        x=x*parameter_estimates[i]
        
        # Fill no data with zeros.
        x=x.filled(0)
        
        # Add predictor term to the response estimate.
        y+=x
    
    
    # Mask out where the array is unchanged from the intercept value (this may remove some valid pixels in addition to masked out pixels).
    y=np.ma.masked_where(y==intercept,y)
    
    # Backtransform the response if asked for.
    if response_backtransformation != 'None':
        y=eval(response_backtransformation)
    
    # Delete datasource.
    del ds
    
    # Return array of predicted values.
    return y

# Description:
    # Applies a linear model to a raster using the specified bands and return a NumPy array of predicted values.

# Required formatting:
    # The directory to a raster image must be provided.

# Arguments:
    # raster: The directory to a raster image.
    # predictor_band_numbers: A list of band numbers in the raster image to be used as predictors in a linear model.
    # parameter_estimates: A list of parameter estimates which the predictor bands will be multiplied by (after transforming, if applicable). The order of the parameter estimates list must match the order of the predictor band numbers list.
    # intercept: The intercept value of a linear model.
    # predictor_transformations: A list of strings which each specify the transformation that should be applied to the bands before multiplying by the parameter estimates. The predictor variable should be represented with an 'x'. For example, a natural logarithm transformation should be written as 'numpy.log(x)'. If no transformation should be applied, then use 'None'.
    # response_backtransformation (default = 'None'): A string which specifies a backtransformation to be applied to the response predictions. The response variable should be represented with a 'y'. For example, if the linear model reponse variable was transformed with the natural logarithm, then 'numpy.exp(y)' would raise the predicted reponse values above the natural number to backtransform the predicted values to the original scale. If no backtransformation should be applied, then use 'None'.
    # mask_by_cloud (default = False, alternative option = specify the Landsat quality assessment band number): If the raster band number of a Landsat BQA (quality assessment) band is provided, then pixels identified using the Landsat BQA band as being covered by cloud will be masked out of the predicted values. If False, clouded pixels will not be masked out.
    # mask_by_NoData (default = False, alternative option = specify a no data value): If a no data value is specified, no data pixels with the specified value will be masked out of each predictor band. So if a pixel contains this no data value in any of its bands, then this pixel will be masked out of the predicted response values array. If False, no data pixels will not be masked out.

###########################
### apply_std_stretch() ###
###########################

# Function:

def apply_std_stretch(raster,rgb,n=2,NoData_value='None'):
    ds=gdal.Open(raster) # Open TIF file.
    if not ds: # Check to make sure file was opened.
        raise IOError('Could not open raster')
    red=ds.GetRasterBand(rgb[0]).ReadAsArray() # Read in band for red color gun.
    green=ds.GetRasterBand(rgb[1]).ReadAsArray() # Read in band for green color gun.
    blue=ds.GetRasterBand(rgb[2]).ReadAsArray() # Read in band for blue color gun.
    
    if NoData_value != 'None': # If a no data value is specified.
        red=np.ma.masked_where(red==NoData_value,red) # Mask out no data value from red band.
        green=np.ma.masked_where(green==NoData_value,green) # Mask out no data value from green band.
        blue=np.ma.masked_where(blue==NoData_value,blue) # Mask out no data value from blue band.
    
    red_mean, red_d = red.mean(), red.std() * n # Get mean and n * std of red band.
    
    red_min = math.floor(max(red_mean - red_d, red.min())) # Get a new red min value.
    red_max = math.ceil(min(red_mean + red_d, red.max())) # Get a new red max value.
    
    red = np.clip(red, red_min, red_max) # Set values higher than new red max value to new red max value and values lower than new red min value to new red min value.
    
    red = (red - red.min()) / (red_max - red_min) # Rescale the red band values.
    
    green_mean, green_d = green.mean(), green.std() * n # Get mean and n * std of green band.
        
    green_min = math.floor(max(green_mean - green_d, green.min())) # Get a new green min value.
    green_max = math.ceil(min(green_mean + green_d, green.max())) # Get a new green max value.
    
    green = np.clip(green, green_min, green_max) # Set values higher than new green max value to new green max value and values lower than new green min value to new green min value.
    
    green = (green - green.min()) / (green_max - green_min) # Rescale the green band values.
    
    blue_mean, blue_d = blue.mean(), blue.std() * n # Get mean and n * std of blue band.
    
    blue_min = math.floor(max(blue_mean - blue_d, blue.min())) # Get a new blue min value.
    blue_max = math.ceil(min(blue_mean + blue_d, blue.max())) # Get a new blue max value.
    
    blue = np.clip(blue, blue_min, blue_max) # Set values higher than new blue max value to new blue max value and values lower than new blue min value to new blue min value.
    
    blue = (blue - blue.min()) / (blue_max - blue_min) # Rescale the blue band values.
    
    stretched_bands=np.dstack((red,green,blue)) # Stack the rgb bands.
    return stretched_bands # Return the stacked bands.

# Description:
    # Applies a standard deviation stretch to a multiband raster image and returns a stacked NumPy array of the specified bands, which are assigned to the red, green, and blue color guns, respectively, when plotted with matplotlib's pyplot.

# Required formatting:
    # The directory to a raster image must be provided.

# Arguments:
    # raster: The directory to a raster image.
    # rgb: A list of three band numbers. The first band number will be incorporated into the stacked NumPy array as the red color gun, the second band number as the green color gun, and the third band number as the blue color gun. The order which band numbers are specified determines the color gun which matplotlib's pyplot will use to symbolize the band when plotted.
    # n (default = 2): The number of standard devations to apply the stretch.
    # NoData_value (default = 'None'): If a no data value is specified, then no data pixels will be masked out of the the raster bands.
